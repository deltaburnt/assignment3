package assignment3;

import java.util.ArrayList;

/**
 * Represents the current game board
 * @author Cassidy
 *
 */

public class Board
{
	private ArrayList<Code> guessList = new ArrayList<Code>();
	private ArrayList<Result> resultList = new ArrayList<Result>();
	
	/**
	 * Get the number of unique guesses the user has made
	 * @return number of guesses
	 */
	public int getNumGuesses()
	{
		return guessList.size();
	}
	
	/**
	 * Adds a new guess to the board
	 * @param newCode guess made by user
	 * @param newResult result of the user's guess
	 * @return whether or not this guess is unique
	 */
	public boolean addGuess(Code newCode, Result newResult)
	{
		if (!guessList.contains(newCode))
		{
			guessList.add(newCode);
			resultList.add(newResult);
			return true;
		}
		
		return false;
	}
	
	/**
	 * Get's the i'th code on the board
	 * @param index
	 * @return The i'th guess
	 */
	public Code getCode(int index)
	{
		return guessList.get(index);
	}
	
	/**
	 * Get's the result for the i'th guess on the board
	 * @param index
	 * @return The i'th result
	 */
	public Result getResult(int index)
	{
		return resultList.get(index);
	}
}
