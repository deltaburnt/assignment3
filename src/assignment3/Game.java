package assignment3;

import java.util.ArrayList;
import java.util.Random;

import assignment3.GameInput.InputType;
import assignment3.GameInput.InvalidInputException;

/**
 * Main Game class. Controls logic flow of the game.
 * @author Cassidy
 *
 */

public class Game
{
	/**
	 * Number of guesses the user gets before losing.
	 */
	public static final int NUM_GUESSES = 15;
	
	/**
	 * Number of pegs in the secret code.
	 */
	public static final int CODE_LENGTH = 5;

	private boolean debugMode;
	private Code secretCode = new Code();
	
	private Board board = new Board();

	/**
	 * Makes a new mastermind game. New object made for each game played.
	 * @param debugMode when true outputs the secret code to the user.
	 */
	Game(boolean debugMode)
	{
		this.debugMode = debugMode;
	}
	
	/**
	 * Runs the game
	 */
	public void runGame()
	{
		generateCode();
		GameInput input = new GUIGameInput();
		GameOutput output = new ConsoleGameOutput();
		
		while(board.getNumGuesses() < NUM_GUESSES)
		{
			if(debugMode) { output.outputDebugCode(secretCode); }
			
			output.outputPrompt(board.getNumGuesses());
			
			try
			{
				input.getInput();
			} catch (InvalidInputException e) {
				output.outputError();
				continue;
			}
			
			if(input.getInputType() == InputType.HISTORY)
			{
				output.outputHistory(board);
				continue;
			}
			
			Code inputCode = input.getInputCode();
			if(inputCode.equals(secretCode))
			{
				output.outputWin();
				return;
			}
			
			Result result = new Result(inputCode, secretCode);
			
			if(board.addGuess(inputCode, result))
			{
				output.outputResult(result);
			}
			else
			{
				output.outputDuplicateGuess();
			}
		}
		
		output.outputLose(secretCode);
	}
	
	private void generateCode()
	{
		Random rand = new Random();
		for(int i = 0; i < Game.CODE_LENGTH; i++)
		{
			Color randColor = Color.values()[rand.nextInt(Color.values().length)]; // Choose random color
			Peg newPeg = new Peg(randColor);
			secretCode.addPeg(newPeg);
		}
	}
}
