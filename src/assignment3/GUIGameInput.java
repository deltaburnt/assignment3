package assignment3;

import javax.swing.JOptionPane;

import assignment3.Color.InvalidColorException;

public class GUIGameInput implements GameInput
{
	private Code inputCode;
	private InputType inputType;
	private String userInput;
	
	@Override
	public void getInput() throws InvalidInputException
	{
		userInput = JOptionPane.showInputDialog("Please input your guess (or type 'history').");
		
		if(userInput == null)
		{
			throw new InvalidInputException();
		}
		
		if(userInput.equalsIgnoreCase("history"))
		{
			inputType = InputType.HISTORY;
			return;
		}
		
		if(userInput.length() != Game.CODE_LENGTH)
		{
			throw new InvalidInputException();
		}
		
		inputCode = new Code();
		
		try
		{
			for(int i = 0; i < Game.CODE_LENGTH; i++)
			{
				Color c = Color.getColor(userInput.charAt(i));
				Peg newPeg = new Peg(c);
				inputCode.addPeg(newPeg);
			}
		} catch (InvalidColorException e) {
			throw new InvalidInputException();
		}
		
		inputType = InputType.CODE;
	}

	@Override
	public InputType getInputType()
	{
		return inputType;
	}

	@Override
	public Code getInputCode()
	{
		return inputCode;
	}

	@Override
	public boolean getBooleanInput()
	{
		int result = JOptionPane.showConfirmDialog(null, "Play again?", "", JOptionPane.YES_NO_OPTION);
		return (result == JOptionPane.YES_OPTION) ? true : false;
	}

}
