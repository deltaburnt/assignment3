/* EE422C Assignment 1 */
/* Student Name: Cassidy Burden (eid: cab5534),  Robert Ly, Lab Section: Xavier (Th 9:30-11:00)*/

package assignment3;

import assignment3.GameInput.InvalidInputException;

public class A3Driver
{
	/**
	 * Determines if debug mode is on or off.
	 */
	public static final boolean DEBUG_MODE = true;

	public static void main(String[] args)
	{
		boolean playGame = true;

		GameInput input = new GUIGameInput();
		GameOutput output = new ConsoleGameOutput();

		output.outputRules();
		
		while(playGame)
		{
			Game mastermind = new Game(A3Driver.DEBUG_MODE);
			mastermind.runGame();
			
			output.outputPlayAgainPrompt();
			while(true)
			{
				try
				{
					playGame = input.getBooleanInput();
					break;
				} catch(InvalidInputException e) {
					output.outputError();
					output.outputPlayAgainPrompt();
				}
			}
		}
	}
}
