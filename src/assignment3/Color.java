package assignment3;

/**
 * Represents one of the 6 valid colors a Peg can take.
 * @author Cassidy
 *
 */

public enum Color
{		
	BLUE ('B'),
	GREEN ('G'),
	MAROON ('M'),
	ORANGE ('O'),
	PURPLE ('P'),
	RED ('R'),
	YELLOW ('Y');
	
	private final char colorCode;
	
	Color(char colorCode)
	{
		this.colorCode = colorCode;
	}
	
	public String toString()
	{
		return this.name().charAt(0) + this.name().toLowerCase().substring(1);
	}
	
	/**
	 * Return a Color object from char according to first letter
	 * @param colorCode first letter of the color
	 * @return the corresponding color
	 * @throws InvalidColorException
	 */
	static Color getColor(char colorCode) throws InvalidColorException
	{
		for(Color c : Color.values())
		{
			if(colorCode == c.colorCode)
			{
				return c;
			}
		}
		
		throw new InvalidColorException();
	}

	public static class InvalidColorException extends Exception
	{
		
	}
}