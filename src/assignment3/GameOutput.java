package assignment3;

import java.util.ArrayList;

public interface GameOutput
{
	/**
	 * Output rules of the game
	 */
	public void outputRules();
	
	/**
	 * Output prompt for user input
	 * @param numGuesses the number of guesses already made
	 */
	public void outputPrompt(int numGuesses);
	
	/**
	 * Output error message for invalid input
	 */
	public void outputError();
	
	/**
	 * Outputs the white and black result pegs for a guess
	 * @param result result pegs for the guess
	 */
	public void outputResult(Result result);
	
	/**
	 * Outputs secret code, used for debugging
	 * @param secretCode secret code for this game
	 */
	public void outputDebugCode(Code secretCode);
	
	/**
	 * Output loss statement telling user the code they were guessing
	 * @param secretCode secret code for this game
	 */
	public void outputLose(Code secretCode);
	
	/**
	 * Output win statement telling user they won the game
	 */
	public void outputWin();
	
	/**
	 * Output history of all guesses and their corresponding results
	 * @param board the current game board
	 */
	public void outputHistory(Board board);
	
	/**
	 * Output yes/no prompt if user wants to play again
	 */
	public void outputPlayAgainPrompt();
	
	/**
	 * Output when the user inputs a guess already entered before
	 */
	public void outputDuplicateGuess();
	
	/**
	 * Clear all output
	 */
	public void clearOutput();
}
