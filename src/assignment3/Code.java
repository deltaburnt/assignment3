package assignment3;

import java.util.ArrayList;

/**
 * The code class represents an input code such as "RRRR"
 * @author Cassidy
 *
 */

public class Code
{
	private ArrayList<Peg> pegList = new ArrayList<Peg>();
	
	/**
	 * Add peg to the current code.
	 * @param newPeg peg to be added
	 */
	public void addPeg(Peg newPeg)
	{
		pegList.add(newPeg);
	}
	
	/**
	 * Get peg from index i in the code.
	 * @param i peg index
	 * @return peg at index i
	 */
	public Peg getPeg(int i)
	{
		return pegList.get(i);
	}
	
	/**
	 * Returns all pegs in the code.
	 * @return an ArrayList of all pegs
	 */
	public ArrayList<Peg> getPegList()
	{
		return pegList;
	}
	
	public boolean equals(Object otherCodeObj)
	{
		if(!(otherCodeObj instanceof Code)) { return false; }
		
		Code otherCode = (Code) otherCodeObj;
		
		if(pegList.size() != otherCode.getPegList().size()) { return false; }
		
		ArrayList<Peg> otherPegList = otherCode.getPegList();
		
		for(int i = 0; i < pegList.size(); i++)
		{
			if(!pegList.get(i).equals(otherPegList.get(i))) { return false; }
		}
		
		return true;
	}
	
	public String toString()
	{
		String stringRep = "";
		for(int i = 0; i < Game.CODE_LENGTH; i++)
		{
			stringRep += pegList.get(i).toString().charAt(0); // Print out first char of the Peg string
		}
		
		return stringRep;
	}
}
