package assignment3;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;

/**
 * Console output implementation. For playing without a GUI.
 * @author Cassidy
 *
 */

public class ConsoleGameOutput implements GameOutput
{

	@Override
	public void outputRules()
	{
		System.out.println("Welcome to mastermind! You have " + Game.NUM_GUESSES +
		                   " tries to guess a " + Game.CODE_LENGTH + " length code.");
		System.out.println("You can choose from the following colors:");
		
		System.out.print(Color.values()[0]);
		for(int i = 1; i < Color.values().length; i++)
		{
			System.out.print(", " + Color.values()[i]);
		}
		
		System.out.println("\n");
	}

	@Override
	public void outputPrompt(int numGuesses)
	{
		System.out.println("Guess " + (numGuesses + 1) + "/" + Game.NUM_GUESSES + ".\n");
	}

	@Override
	public void outputError()
	{
		System.out.println("Your input was invalid, please try again.");
		System.out.println();
	}
	

	@Override
	public void outputResult(Result result)
	{
		int numWhite = result.getWhiteCount();
		int numBlack = result.getBlackCount();
		JOptionPane.showMessageDialog(null, "Result: " + numBlack + " Black, " + numWhite + " White");
	}

	@Override
	public void outputLose(Code code)
	{
		System.out.println("Sorry, you lose! The secret code was: " + code + "\n\n");
	}
	
	@Override
	public void outputWin()
	{
		System.out.println("That's right! You win!\n\n");
	}
	

	@Override
	public void outputHistory(Board board)
	{
		if(board.getNumGuesses() == 0)
		{
			JOptionPane.showMessageDialog(null, "You have made no guesses.");
			return;
		}
		
		String historyString = "Your guessing history is:\n";
		historyString += "Guess\t\t# Black\t\t# White\n";
		
		for(int i = 0; i < board.getNumGuesses(); i++)
		{
			int numWhite = board.getResult(i).getWhiteCount();
			int numBlack = board.getResult(i).getBlackCount();

			historyString += board.getCode(i);
			historyString += ("\t\t" + numBlack + "\t\t" + numWhite + "\n");
		}
		
		JOptionPane.showMessageDialog(null, new JTextArea(historyString));
		
	}
	
	@Override
	public void outputDebugCode(Code secretCode)
	{
		System.out.println("DEBUG: The secret code is: " + secretCode.toString());
	}

	@Override
	public void clearOutput()
	{
		for(int i = 0; i < 50; i++)
		{
			System.out.println();
		}
	}

	@Override
	public void outputPlayAgainPrompt()
	{
		System.out.println("Would you like to play again (Y/N)?");
	}
	
	@Override public void outputDuplicateGuess()
	{
		System.out.println("You have already made that guess! Please try again.\n");
	}

}
