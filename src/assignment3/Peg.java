package assignment3;

/**
 * Represents an individual Peg in a Code
 * @author Cassidy
 *
 */

public class Peg
{
	public Color pegColor;
	
	Peg(Color c)
	{
		this.pegColor = c;
	}
	
	@Override
	public boolean equals(Object otherPeg)
	{
		if(otherPeg instanceof Peg)
		{
			return this.pegColor == ((Peg) otherPeg).pegColor;
		}
		
		return false;
	}
	
	@Override
	public int hashCode()
	{
		return pegColor.hashCode();
	}
	
	@Override
	public String toString()
	{
		return pegColor.toString();
	}
}
