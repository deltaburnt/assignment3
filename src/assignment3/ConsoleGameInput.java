package assignment3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import assignment3.Color.InvalidColorException;

/**
 * Console input implementation. For playing without a GUI.
 * @author Cassidy
 *
 */

public class ConsoleGameInput implements GameInput
{
	private BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
	private Code inputCode;
	private InputType inputType;
	private String userInput;

	@Override
	public void getInput() throws InvalidInputException
	{
		try
		{
			userInput = input.readLine();
		} catch (IOException e) {
			throw new InvalidInputException();
		}
		
		if(userInput.equalsIgnoreCase("history"))
		{
			inputType = InputType.HISTORY;
			return;
		}
		
		if(userInput.length() != Game.CODE_LENGTH)
		{
			throw new InvalidInputException();
		}
		
		inputCode = new Code();
		
		try
		{
			for(int i = 0; i < Game.CODE_LENGTH; i++)
			{
				Color c = Color.getColor(userInput.charAt(i));
				Peg newPeg = new Peg(c);
				inputCode.addPeg(newPeg);
			}
		} catch (InvalidColorException e) {
			throw new InvalidInputException();
		}
		
		inputType = InputType.CODE;
	}

	@Override
	public InputType getInputType()
	{
		return inputType;
	}

	@Override
	public Code getInputCode()
	{
		return inputCode;
	}

	@Override
	public boolean getBooleanInput() throws InvalidInputException
	{
		try
		{
			userInput = input.readLine();
		} catch (IOException e) {
			throw new InvalidInputException();
		}

		if(userInput.matches("[ynYN]"))
		{
			return (userInput.equalsIgnoreCase("Y")) ? true : false;
		}
	
		throw new InvalidInputException();
	}

}
